from typing import Any

import attr
import pandas as pd
import torch
from torch.utils.data.dataset import Dataset
from transformers import AutoTokenizer

@attr.s(kw_only=True, auto_attribs=True)
class SensorData(Dataset):
    file: str
    tokenizer: AutoTokenizer
    transformer: Any = None
    include_labels: bool = True

    def __attrs_post_init__(self):
        self.data: pd.DataFrame = pd.read_csv(self.file, engine="python")

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        elif not isinstance(idx, list):
            idx = [idx]

        # batch encode
        encoding = self.tokenizer.__call__(
            self.data.loc[idx, "features"].tolist(),
            return_tensors="pt",
            padding="max_length",
            truncation=True,
        )

        encoding["input_ids"] = encoding["input_ids"].flatten()
        encoding["attention_mask"] = encoding["attention_mask"].flatten()
        if self.include_labels:
            encoding["labels"] = torch.tensor(
                [self.data.loc[idx, "target"].tolist()]
            ).flatten()
        return encoding
