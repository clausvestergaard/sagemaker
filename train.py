import argparse
import logging
import os
from pathlib import Path

import numpy as np
import torch
from transformers import (
    AutoModelForSequenceClassification,
    AutoTokenizer,
    Trainer,
    TrainingArguments,
)

from sensordata import SensorData

torch.manual_seed(42)
np.random.seed(42)

logger = logging.getLogger(__name__)
logger.setLevel(int(os.environ["SM_LOG_LEVEL"]))


# Load model
def load_trainer(train_data, test_data, args):
    model = AutoModelForSequenceClassification.from_pretrained(args.pretrained_model)

    training_args = TrainingArguments(
        output_dir=args.output_dir,  # output directory
        num_train_epochs=args.num_train_epochs,  # total # of training epochs
        per_device_train_batch_size=args.per_device_train_batch_size,  # batch size per device during training
        per_device_eval_batch_size=args.per_device_eval_batch_size,  # batch size for evaluation
        warmup_steps=args.warmup_steps,  # number of warmup steps for learning rate scheduler
        weight_decay=args.weight_decay,  # strength of weight decay
        #logging_dir=args.logging_dir,  # directory for storing logs
    )

    trainer = Trainer(
        model=model,  # the instantiated 🤗 Transformers model to be trained
        args=training_args,  # training arguments, defined above
        train_dataset=train_data,  # training dataset
        eval_dataset=test_data,  # evaluation dataset
    )
    return trainer


# save model
def _save_model(model, model_dir):
    logger.info("Saving the model.")
    model.save_model(model_dir)


def model_fn(model_dir):
    logger.info("model_fn")
    device = "cuda" if torch.cuda.is_available() else "cpu"
    model = AutoModelForSequenceClassification.from_pretrained(model_dir)
    return model.to(device)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--pretrained-model", type=str, default="distilbert-base-multilingual-cased"
    )

    parser.add_argument("--output-dir", type=str, default=os.environ["SM_MODEL_DIR"])
    parser.add_argument("--num-train-epochs", type=int, default=1)
    parser.add_argument("--per-device-train-batch-size", type=int, default=1)
    parser.add_argument("--per-device-eval-batch-size", type=int, default=2)
    parser.add_argument("--warmup-steps", type=int, default=1)
    parser.add_argument("--weight-decay", type=float, default=0.91)
    parser.add_argument("--logging-dir", type=str, default="./logs")
    parser.add_argument("--train", type=str, default=os.environ["SM_CHANNEL_TRAIN"])
    parser.add_argument("--test", type=str, default=os.environ["SM_CHANNEL_TEST"])

    args = parser.parse_args()
    

    # initialize tokenizer
    tokenizer = AutoTokenizer.from_pretrained(args.pretrained_model)

    
    # load data

    print(args.train)
    print(args.test)
    train_data = SensorData(
        file=Path(args.train).joinpath("train.csv"), 
        tokenizer=tokenizer
    )
    test_data = SensorData(
        file=Path(args.test).joinpath("test.csv"),
        tokenizer=tokenizer,
        include_labels=False,
    )

    # train data
    trainer = load_trainer(train_data, test_data, args)
    trainer.train()
    logging.info("training done")

    
    # save model
    _save_model(trainer, args.output_dir)
